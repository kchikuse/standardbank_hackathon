export default {
  methods: {
    openPage(page, data) {
      this.$router.push({
        name: 'page-' + page,
        params: {
          data
        }
      });
    },
    openHomePage() {
      this.$router.push({
        name: 'home' 
      });
    }
  }
};