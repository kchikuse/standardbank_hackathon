const formatter = new Intl.NumberFormat('en-ZA', {
    minimumFractionDigits: 0
});

const currency = value => 'R ' + formatter.format(value).replace(',', '.');

export { currency };