import Vue from 'vue';
import Router from 'vue-router';
import VueApexCharts from 'vue-apexcharts';
import VuePageTransition from 'vue-page-transition';

import FAB from '@/features/FAB.vue';
import { currency } from '@/filters';
import TopHeader from '@/features/TopHeader.vue';
import Page1 from '@/features/Page1.vue';
import Page2 from '@/features/Page2.vue';
import Page3 from '@/features/Page3.vue';
import Page4 from '@/features/Page4.vue';
import Page5 from '@/features/Page5.vue';
import Page6 from '@/features/Page6.vue';
import Page7 from '@/features/Page7.vue';
import Page8 from '@/features/Page8.vue';
import Page9 from '@/features/Page9.vue';
import Page10 from '@/features/Page10.vue';
import Page11 from '@/features/Page11.vue';
import Home from '@/features/Home.vue';
import Mixins from './mixins';
import App from './App.vue';

Vue.use(Router);
Vue.use(VuePageTransition);
Vue.component('FAB', FAB);
Vue.component('top-header', TopHeader);
Vue.component('apexchart', VueApexCharts);
Vue.filter('currency', currency);
Vue.mixin(Mixins);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/1',
      name: 'page-1',
      component: Page1,
      props: true
    },
    {
      path: '/2',
      name: 'page-2',
      component: Page2,
      props: true
    },
    {
      path: '/3',
      name: 'page-3',
      component: Page3,
      props: true
    },
    {
      path: '/4',
      name: 'page-4',
      component: Page4,
      props: true
    },
    {
      path: '/5',
      name: 'page-5',
      component: Page5,
      props: true
    },
    {
      path: '/6',
      name: 'page-6',
      component: Page6,
      props: true
    },
    {
      path: '/7',
      name: 'page-7',
      component: Page7,
      props: true
    },
    {
      path: '/8',
      name: 'page-8',
      component: Page8,
      props: true
    },
    {
      path: '/9',
      name: 'page-9',
      component: Page9,
      props: true
    },
    {
      path: '/10',
      name: 'page-10',
      component: Page10,
      props: true
    },
    {
      path: '/11',
      name: 'page-11',
      component: Page11,
      props: true
    }
  ]
});

new Vue({
  el: '#app',
  render: h => h(App),
  router
});